import React from "react";
import "../../App.css";
import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import BarChartWeeks from '../graphs/BarChartWeeks';

let localeMonth = new Date().getMonth() + 1; //new Date().getMonth() + 1
let localeYear = new Date().getFullYear();
let localeWeek = weekAndDay(new Date());

function weekAndDay(date) {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday', 'Saturday'],
        prefixes = [1, 2, 3, 4, 5];
    //days[date.getDay()];
    return prefixes[Math.floor(date.getDate() / 7)]

}


export default function MonthOverviewExpenses({ props }) {
    let totalExpensesMonth = 0;
    let totalExpensesWeek = 0;
    let propsFilteredMonth = [];
    let propsFilteredMonthMap = [];
    let propsFilteredWeek = [];
    let propsFilteredWeekMap = [];

    if (props.length > 0) {
        propsFilteredMonth = props.filter((result) => {
            var month = new Date(result.dateAdded).getMonth() + 1;
            var year = new Date(result.dateAdded).getFullYear();
            return localeMonth == month && localeYear == year
        })
    }

    if (propsFilteredMonth.length > 0) {
        propsFilteredMonthMap = propsFilteredMonth.map(result => {
            totalExpensesMonth += result.expensesPrice
        })
    }

    if (props.length > 0) {
        propsFilteredWeek = props.filter((result) => {
            var week = weekAndDay(new Date(result.dateAdded))
            var month = new Date(result.dateAdded).getMonth() + 1;
            var year = new Date(result.dateAdded).getFullYear();
            return localeMonth == month && localeYear == year
        })

    }

    let BargraphChart = {};

    /* ---------------------------- Resets per month ---------------------------- */

    let totalExpensesWeek5 = 0;
    let totalExpensesWeek4 = 0;
    let totalExpensesWeek3 = 0;
    let totalExpensesWeek2 = 0;
    let totalExpensesWeek1 = 0;

    /* ----------------------------------- ... ---------------------------------- */

    if (propsFilteredWeek.length > 0) {
        propsFilteredWeekMap = propsFilteredWeek.map(result => {
            var week = weekAndDay(new Date(result.dateAdded))
            if (week == 5) {

                totalExpensesWeek5 += result.expensesPrice
                return BargraphChart.week5 = totalExpensesWeek2
            }
            else if (week == 4) {

                totalExpensesWeek4 += result.expensesPrice
                return BargraphChart.week4 = totalExpensesWeek2
            }
            else if (week == 3) {

                totalExpensesWeek3 += result.expensesPrice
                return BargraphChart.week3 = totalExpensesWeek3

            }
            else if (week == 2) {

                totalExpensesWeek2 += result.expensesPrice
                return BargraphChart.week2 = totalExpensesWeek2
            }
            else if (week == 1) {

                totalExpensesWeek1 += result.expensesPrice
                return BargraphChart.week1 = totalExpensesWeek1
            }
        })
    }





    //console.log(BargraphChart)








    return (
        <Card className="cardListSmall">
            <Card.Body>
                <Card.Text className="fontSmall">Weekly chart</Card.Text>
                <BarChartWeeks rawData={BargraphChart} />
                <Card.Text className="fontSmall">Month's Expenses: </Card.Text>
                <Card.Text >- PHP {totalExpensesMonth} </Card.Text >
            </Card.Body>
        </Card>
    )
}