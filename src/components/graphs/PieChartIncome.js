import React from 'react'
import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
var randomColor = require('randomcolor')

export default function PieChartIncome({ props }) {
    //console.log(props)

    //let percentage = ((props.income / total.totalIncome) * 100).toFixed(2)
    //console.log(percentage)

    const [category, setCategory] = useState([])
    const [expenses, setExpenses] = useState([])
    const [income, setIncome] = useState([])
    const [randomBGColors, setRandomBGColors] = useState([])

    useEffect(() => {
        if (props.length > 0) {
            setCategory(props.map(element => element.category))
            setIncome(props.map(element => element.income))
            setExpenses(props.map(element => element.expenses))
            setRandomBGColors(props.map(() => randomColor({ luminosity: 'light' })))
        }
    }, [props])


    let list = income;
    if (income.length == 0) {
        list = expenses;
    }

    const data = {
        labels: category,
        datasets: [{
            data: list,
            backgroundColor: randomBGColors
        }]

    }

    const options = {
        legend: {
            fontColor: "white"
        }
    }

    return (
        <>
            <Pie data={data} options={options} />
        </>

    )
}