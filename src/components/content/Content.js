import React from "react";
import "../../App.css";
import classNames from "classnames";
import { Container } from "reactstrap";
import { Switch, Route, useLocation } from "react-router-dom";

import Topbar from "./Topbar";
import Home from "../../pages/home";
import Register from "../../pages/register";
import Login from "../../pages/login";
import Logout from "../../pages/logout";
import Records from './../../pages/records';
import Analytics from './../../pages/analytics';
import { AnimatePresence } from "framer-motion";
import IncomeListScroll from "../scrollbar/IncomeListScroll";
import About from './../../pages/about';
import Profile from './../../pages/profile';


const Content = ({ sidebarIsOpen, toggleSidebar }) => {
  const location = useLocation();

  return (
    <Container
      fluid
      className={classNames("content", { "is-open": sidebarIsOpen })}
    >
      <Topbar toggleSidebar={toggleSidebar} />

      <AnimatePresence exitBeforeEnter initial={false}>
        <Switch location={location} key={location.pathname}>

          <Route exact path="/" component={Home} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/horizontal" component={IncomeListScroll} />
          <Route exact path="/analytics" component={Analytics} />
          <Route exact path="/records" component={Records} />
          <Route exact path="/about" component={About} />
          <Route exact path="/profile" component={Profile} />
        </Switch>
      </AnimatePresence>
    </Container>
  );
};

export default Content;
