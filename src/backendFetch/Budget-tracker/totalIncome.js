import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";

/* ---------------------------- public function ---------------------------- */

export default function fetchTotalIncome(id, token, setTotalIncome) {
  fetch(`${server}/api/users/totalIncome`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      id: id,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      //console.log("fetchTotalIncomeData: ", data);
      setTotalIncome(data);
    });
}
