import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";
import { useState, useEffect, useContext } from "react";

/* ---------------------------- public function ---------------------------- */

export const AutoRender = false;

export default function fetchAddIncome(id, token, income, description, category, setFetchAddIncome) {
    // console.log('id :', id);
    // console.log('category :', category);
    // console.log('description :', description);
    // console.log('income :', income);
    // console.log('token :', token);
    fetch(`${server}/api/users/addIncome`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
            id: id,
            income: income,
            description, description,
            category: category
        }),
    })
        .then((res) => res.json())
        .then((data) => {
            if (data === true) {
                setFetchAddIncome(data);

                Swal.fire({
                    icon: "success",
                    title: "Successfully Added.",
                    text: "Added to Income.",
                });

            } else {
                Swal.fire({
                    icon: "error",
                    title: "Failed.",
                    text: "Fail to add.",
                });
            }


        })


}
