import React from "react";
import ReactApexChart from "react-apexcharts";

export default function SplineChartTrend({ props }) {
    //console.log('props :', props);

    let dateMap = props.map((result) => {
        return result.date
    })
    // console.log('dateMap :', dateMap);
    let balanceMap = props.map((result) => {
        return result.balance
    })
    // console.log('balanceMap :', balanceMap);
    const series = [
        {
            name: "Balance",
            data: balanceMap,
        },

    ];
    const options = {
        dataLabels: {
            enabled: false,
        },
        stroke: {
            curve: "smooth",
        },
        xaxis: {
            type: "datetime",
            categories: dateMap,
            axisTicks: {
                color: "#78909C"
            },
            labels: {
                style: {
                    colors: '#FFF'
                }
            }
        },
        yaxis: {
            axisTicks: {
                borderType: 'solid',
                color: '#78909C',
            },
            labels: {
                style: {
                    colors: '#FFF'
                }
            }
        },


        tooltip: {
            x: {
                format: "dd/MM/yy",
            },
        },
    };

    return (
        <div
            style={{
                backgroundColor: "transparent",
                textAlign: "center",
            }}
        >
            <h3 className="text-white">Balance Trend</h3>
            <ReactApexChart
                options={options}
                series={series}
                type="area"
                height={250}

            />
        </div>
    );
}

