import React from "react";
import "../../App.css";
import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";


export default function CardListCategories({ listProp }) {
  // console.log(courseProp);
  return (
    <Col>
      <Card className="cardListCategory">
        <Card.Body className="cardBodyCategory">
          <Row>
            <Col lg="6">
              <Card.Title>{listProp.category}</Card.Title>
              <Card.Text className="fontSmall">Total: PHP {listProp.income || listProp.expenses}</Card.Text>
            </Col>
            <Col lg="6" className="cardPercentage">
              {listProp.income ?
                <Card.Text> {((listProp.income / listProp.totalIncome) * 100).toFixed(2)} %</Card.Text>

                :
                <></>
              }
              {listProp.expenses ?
                <Card.Text > {((listProp.expenses / listProp.totalExpenses) * 100).toFixed(2)} % </Card.Text>
                :
                <></>
              }
            </Col>
          </Row>




        </Card.Body>
      </Card>
    </Col >
  );
}
