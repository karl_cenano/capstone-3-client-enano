import React from "react";
import { useState, useEffect, useContext } from "react";



import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';




const columnsIncome = [
    {
        name: "description",
        label: "description",
        options: {
            filter: true,
            sort: false,
        }
    },

    {
        name: "category",
        label: "Category",
        options: {
            filter: true,
            sort: true,

        }
    },
    {
        name: "income",
        label: "Income",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "expenses",
        label: "Expenses",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "month",
        label: "Month",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "day",
        label: "Day",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "year",
        label: "Year",
        options: {
            filter: true,
            sort: true,
        }
    }
];

export default function NiceTable({ props }) {
    //console.log('props :', props);


    let getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                root: {
                    backgroundColor: "#FF0000"
                }
            }
        },
        palette: {
            type: 'dark',

            primary: {
                light: '#757ce8',
                main: '#3f50b5',
                dark: '#002884',
                contrastText: '#fff',
            },
            secondary: {
                light: '#ff7961',
                main: '#f44336',
                dark: '#ba000d',
                contrastText: '#000',
            },
        },


    })




    const options = {
        filterType: 'checkbox',
    };

    return (
        <>
            <MuiThemeProvider theme={getMuiTheme()}>
                <MUIDataTable

                    title={"Records History"}
                    data={props}
                    columns={columnsIncome}
                    options={options}
                    selectableRowsHeader={false}

                />
            </MuiThemeProvider>
        </>

    )
}