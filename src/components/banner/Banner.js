/*//^ ------------------------------ declarations ------------------------------ */

import React from "react";
import { Jumbotron, Button, Row, Col, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../../App.css";

/*//^ -------------------------------- component ------------------------------- */

export default function Banner({ dataProp }) {
  //component
  const { title, description, motto, destination, label } = dataProp;

  /*//^ --------------------------------- return --------------------------------- */

  return (
    <>
      <Row>
        <Col>
          <Jumbotron className="Jumbotron">
            {/* still jsx */}
            <Row>
              <Col md="5" className="justify-content-md-center">
                <h1 className="text-white jumboTitle">{title}</h1>
                <p className="text-white">{description}</p>
                <p className="text-white">{motto}</p>
                <Link className="jumboLink" to={destination}>
                  {label}{" "}
                </Link>
              </Col>
              <Col md="1" className="justify-content-md-center"></Col>
              <Col
                md="6"
                className="justify-content-md-center jumboImage"
              ></Col>
            </Row>
          </Jumbotron>
        </Col>
      </Row>
    </>
  );
}
