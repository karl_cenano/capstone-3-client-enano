import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";
import { useState, useEffect, useContext } from "react";

/* ---------------------------- public function ---------------------------- */

export const AutoRender = false;

export default function fetchUserDetails(token, setUserDetails) {

    fetch(`${server}/api/users/details`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    })
        .then((res) => res.json())
        .then((data) => {
            setUserDetails(data);
        })
}