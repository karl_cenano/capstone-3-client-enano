import React from 'react'
import Swal from "sweetalert2";
import server from "./server";
import { Redirect } from "react-router-dom";
/* ----------------------------- export function ---------------------------- */

export default function fetchRegister(
  firstName,
  lastName,
  email,
  mobileNo,
  password1
) {
  return fetch(`${server}/api/users/email-exists`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data === false) {
        localRegister(firstName, lastName, email, mobileNo, password1);
      } else {
        Swal.fire({
          icon: "error",
          title: "Registration failed.",
          text: "Email Already Registered.",
        });
      }
    });
}

/* ----------------------------- local function ----------------------------- */

function localRegister(firstName, lastName, email, mobileNo, password1) {
  fetch(`${server}/api/users`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      email: email,
      mobileNo: mobileNo,
      password: password1,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      // console.log('data1111 :', data);
      if (data === true) {
        Swal.fire({
          icon: "success",
          title: "Successfully Registered.",
          text: "Thank you for registering",
        });
        window.location.replace('/login')
        //redirect to login page
        //Router.push('/login')
      } else {
        // error in creating registration
        Swal.fire({
          icon: "error",
          title: "Register failed.",
          text: "try again.",
        });
      }
    });
}
