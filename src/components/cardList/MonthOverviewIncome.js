import React from "react";
import "../../App.css";
import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import BarChartWeeks from '../graphs/BarChartWeeks';

let localeMonth = new Date().getMonth() + 1; //new Date().getMonth() + 1
let localeYear = new Date().getFullYear();
let localeWeek = weekAndDay(new Date());


function weekAndDay(date) {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday', 'Saturday'],
        prefixes = [1, 2, 3, 4, 5];
    //days[date.getDay()];
    return prefixes[Math.floor(date.getDate() / 7)]

}


export default function MonthOverviewIncome({ props }) {
    //console.log('props :', props);


    let totalIncomeMonth = 0;
    let totalIncomeWeek = 0;
    let propsFilteredMonth = [];
    let propsFilteredWeek = [];
    let propsFilteredMonthMap = [];
    let propsFilteredWeekMap = [];



    //pros:array of incomelist 
    if (props.length > 0) {
        propsFilteredMonth = props.filter((result) => {
            var month = new Date(result.dateAdded).getMonth() + 1;
            var year = new Date(result.dateAdded).getFullYear();
            return localeMonth == month && localeYear == year
        })
    }
    //console.log('propsFiltered :', propsFiltered);
    if (propsFilteredMonth.length > 0) {
        propsFilteredMonthMap = propsFilteredMonth.map(result => {
            totalIncomeMonth += result.incomePrice
        })
    }

    if (props.length > 0) {
        propsFilteredWeek = props.filter((result) => {
            var week = weekAndDay(new Date(result.dateAdded))
            var month = new Date(result.dateAdded).getMonth() + 1;
            var year = new Date(result.dateAdded).getFullYear();
            return localeMonth == month && localeYear == year
        })
        // console.log('propsFilteredWeek', propsFilteredWeek)
    }



    let BargraphChart = {};

    /* ---------------------------- Resets per month ---------------------------- */

    let totalIncomeWeek5 = 0;
    let totalIncomeWeek4 = 0;
    let totalIncomeWeek3 = 0;
    let totalIncomeWeek2 = 0;
    let totalIncomeWeek1 = 0;
    /* ----------------------------------- ... ---------------------------------- */

    if (propsFilteredWeek.length > 0) {
        propsFilteredWeekMap = propsFilteredWeek.map(result => {

            var week = weekAndDay(new Date(result.dateAdded))
            if (week == 5) {

                totalIncomeWeek5 += result.incomePrice
                return BargraphChart.week5 = totalIncomeWeek2
            }
            else if (week == 4) {

                totalIncomeWeek4 += result.incomePrice
                return BargraphChart.week4 = totalIncomeWeek2
            }
            else if (week == 3) {

                totalIncomeWeek3 += result.incomePrice
                return BargraphChart.week3 = totalIncomeWeek3

            }
            else if (week == 2) {

                totalIncomeWeek2 += result.incomePrice
                return BargraphChart.week2 = totalIncomeWeek2
            }
            else if (week == 1) {

                totalIncomeWeek1 += result.incomePrice
                return BargraphChart.week1 = totalIncomeWeek1
            }
        })
        //console.log(BargraphChart)
    }









    return (
        <Card className="cardListSmall">
            <Card.Body>
                <Card.Text className="fontSmall">Weekly chart</Card.Text>
                <BarChartWeeks rawData={BargraphChart} />
                <Card.Text className="fontSmall">Month's Income: </Card.Text>
                <Card.Text >+ PHP {totalIncomeMonth} </Card.Text >
            </Card.Body>
        </Card>
    )
}