import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";

/* ---------------------------- public function ---------------------------- */

export default function fetchIncomeList(id, token, setIncomeList) {
  fetch(`${server}/api/users/incomeList`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      id: id,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      //console.log("fetchsetIncomeList: ", data);
      setIncomeList(data);
    });
}
