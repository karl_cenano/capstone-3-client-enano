import React, { useState, useEffect } from "react";

/* --------------------------- bootstrap and react -------------------------- */
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import { Footer } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

/* ------------------------------- components ------------------------------- */
import SideBar from "./components/sidebar/SideBar";
import Content from "./components/content/Content";
/* ----------------------------------- css ---------------------------------- */
import "./App.css";
/* ---------------------------- context provider ---------------------------- */
import { UserProvider } from "./userContext";
import { useMedia } from "react-media";
//~ -------------------------------- function -------------------------------- */

const App = () => {

  /* ---------------------------------- media---------------------------------- */

  const GLOBAL_MEDIA_QUERIES = {
    small: "(max-width: 599px)",
    medium: "(max-width: 1365px)",
    large: "(min-width:1366px)"
  };
  const matches = useMedia({ queries: GLOBAL_MEDIA_QUERIES });
  // console.log('matches.medium :', matches.medium);



  /* --------------------------------- sidebar -------------------------------- */

  const [sidebarIsOpen, setSidebarOpen] = useState(true);

  const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);
  const toggleSidebarByScreen = () => setSidebarOpen(false);

  useEffect(() => {
    if (matches.medium) {
      setSidebarOpen(false)
    } else if (matches.large) {
      setSidebarOpen(true)
    }



  }, [matches])

  // console.log('sidebarIsOpen :', sidebarIsOpen);

  /* ------------------------------ local storage ----------------------------- */

  const [user, setUser] = useState({
    email: localStorage.getItem("email"),
    id: localStorage.getItem("id"),
    token: localStorage.getItem("token"),
  });
  const unsetUser = () => {
    localStorage.clear();
    setUser({
      email: null,
      id: null,
      token: null,
    });
  };

  //^ --------------------------------- return --------------------------------- */
  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <div className="App wrapper">
            <SideBar toggle={toggleSidebar} isOpen={sidebarIsOpen} />
            <Content
              toggleSidebar={toggleSidebar}
              sidebarIsOpen={sidebarIsOpen}
            />
          </div>
        </Router>
      </UserProvider>
    </>
  );
};

export default App;
