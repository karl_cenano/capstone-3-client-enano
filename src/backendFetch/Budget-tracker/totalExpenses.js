
import server from "../server";


/* ---------------------------- public function ---------------------------- */

export default function fetchTotalExpenses(id, token, setTotalExpenses) {
  fetch(`${server}/api/users/totalExpenses`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      id: id,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      //console.log("fetchTotalExpensesData: ", data);
      setTotalExpenses(data);

    });
}
