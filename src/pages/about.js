import React from "react";
import "./../App.css";
import {
    Jumbotron
} from "react-bootstrap";
import { motion } from "framer-motion";


export default function About() {

    /* ------------------------------- Animations ------------------------------- */

    const pageVariants = {
        initial: {
            opacity: 1,
            x: "100vw",
            scale: 1,
        },
        in: {
            opacity: 1,
            x: 0,
            scale: 1,
        },
        out: {
            opacity: 1,
            x: "100vw",
            scale: 1,
        },
    };

    const pageTransition = {
        type: "tween",
        ease: "easeInOut",
        duration: 0.5,
    };

    const pageStyle = {
        position: "relative",
    };

    return (

        <motion.div
            style={pageStyle}
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={pageTransition}
        >
            <Jumbotron className="aboutJumbotron">
                <div className="about-section">
                    <h1 className="text-align-center">About The Developer</h1>
                    <br />
                    <p>Hello! I'm Karl. I'm a Full-Stack Web Developer from the Philippines </p>
                    <p>Feel free to check my budget-tracking web-app.</p>
                </div>
            </Jumbotron>
        </motion.div>

    )
}
