import React from "react";
import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'

let localeMonth = new Date().getMonth() + 1; //new Date().getMonth() + 1
let localeYear = new Date().getFullYear();

export default function BarChartMonths({ props }) {
    // console.log('props :', props);

    let totalIncomeMonth = 0;
    let propsFilteredYear = [];

    let propsFilteredMonthMap = [];
    let summaryMonths = [];
    let arrayOfObjectstoObjects = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
        9: 0,
        10: 0,
        11: 0,
        12: 0
    };

    if (props.length > 0) {
        propsFilteredYear = props.filter((result) => {
            var year = new Date(result.dateAdded).getFullYear();
            return localeYear == year
        })
    }
    //console.log('propsFilteredYear :', propsFilteredYear);

    //! need total per month 

    // console.log('propsFiltered :', propsFiltered);
    if (propsFilteredYear.length > 0) {
        propsFilteredMonthMap = propsFilteredYear.map(result => {
            //console.log('result :', result);
            var month = new Date(result.dateAdded).getMonth() + 1;
            return (
                {
                    month: month,
                    price: result.incomePrice || result.expensesPrice
                }
            )

        })
    }
    //console.log('propsFilteredMonthMap :', propsFilteredMonthMap);

    //total per month
    if (propsFilteredMonthMap.length > 0) {
        summaryMonths = Array.from(propsFilteredMonthMap.reduce(
            (m, { month, price }) => m.set(month, (m.get(month) || 0) + price), new Map
        ), ([month, price]) => ({ month, price }))
    };

    //console.log('summaryMonths :', summaryMonths);

    if (summaryMonths.length > 0) {
        for (var i = 0; i < summaryMonths.length; i++) {
            arrayOfObjectstoObjects[summaryMonths[i].month] = summaryMonths[i].price;
        }
    }
    console.log('arrayOfObjectstoObjects :', arrayOfObjectstoObjects);



    const data = {
        labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
            label: 'month',
            backgroundColor: 'purple',
            borderColor: 'purple',
            borderWidth: 1,
            hoverBackgroundColor: 'lightblue',
            hoverBorderColor: 'black',
            data: [
                arrayOfObjectstoObjects[1],
                arrayOfObjectstoObjects[2],
                arrayOfObjectstoObjects[3],
                arrayOfObjectstoObjects[4],
                arrayOfObjectstoObjects[5],
                arrayOfObjectstoObjects[6],
                arrayOfObjectstoObjects[7],
                arrayOfObjectstoObjects[8],
                arrayOfObjectstoObjects[9],
                arrayOfObjectstoObjects[10],
                arrayOfObjectstoObjects[11],
                arrayOfObjectstoObjects[12],

            ]
        }]
    }

    const options = {
        legend: {
            display: false
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        display: true,
                        maxTicksLimit: 5,
                        fontColor: 'white'

                    },
                    gridLines: {
                        display: true
                    },



                }
            ],
            xAxes: [{
                // categoryPercentage: 1.0,
                barPercentage: 1,
                gridLines: {
                    display: false
                },
                ticks: {
                    fontColor: 'white'
                }

            }],

        }
    }


    return (
        <Bar data={data} options={options} />
    )
}