import React from "react";
import '../App.css'
import { useState, useEffect, useContext } from "react";
import fetchUserDetails from '../backendFetch/Budget-tracker/userProfile'
import UserContext from "../userContext";
import {
    Jumbotron,
} from "react-bootstrap";
import { motion } from "framer-motion";

function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
}


export default function Profile() {
    const { user, setUser } = useContext(UserContext);
    const [userDetails, setUserDetails] = useState({})

    const pageVariants = {
        initial: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
        in: {
            opacity: 1,
            x: 0,
            scale: 1,
        },
        out: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
    };
    const pageTransition = {
        type: "tween",
        ease: "easeInOut",
        duration: 0.5,
    };

    const pageStyle = {
        position: "relative",
    };


    useEffect(() => {
        fetchUserDetails(user.token, setUserDetails);
    }, []);

    // console.log('userDetails :', userDetails);


    return (
        <motion.div
            style={pageStyle}
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={pageTransition}
        >
            <Jumbotron className="text-align-center text-white JumbotronProfile ">
                <h1>Profile</h1>
                <br />
                <h2>{userDetails.firstName ? titleCase(userDetails.firstName) : ""} {userDetails.lastName ? titleCase(userDetails.lastName) : ""}</h2>
                <h3>Email: {userDetails.email}</h3>
                <h3>Mobile: {userDetails.mobileNo}</h3>
            </Jumbotron>
        </motion.div>
    )
}
