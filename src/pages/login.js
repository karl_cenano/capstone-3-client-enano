/* --------------------------------- imports -------------------------------- */
import React from "react";
import '../App.css'
import { useState, useEffect, useContext } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

import fetchLogin from "../backendFetch/login";
import UserContext from "../userContext";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
//import { browserHistory } from "react-router";
//import { browserHistory } from 'react-router';
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import server from '../backendFetch/server'


function redirect() {
  window.location.replace('/')
}



export default function Login() {
  //global props

  const { user, setUser } = useContext(UserContext);


  /* ------------------------------ google login ------------------------------ */
  function authenticateGoogleToken(response) {

    //response - google's response with our tokenId to be used to authenticate our google login user
    //response comes from google along with our user's details and token for authentication.

    // console.log('response :', response);

    //send our google login user's token id to the API server for 
    //authentication and retrieval of our own App's token to be used for logging into our App.

    //at this point, this URL endpoint: /verify-google-id-token hasn't been created yet.
    //Refer to part 2 of this Google Login step by step our server side.

    fetch(`${server}/api/users/verify-google-id-token`, {

      method: 'POST',
      headers: {

        'Content-Type': 'application/json'

      },
      body: JSON.stringify({

        tokenId: response.tokenId

      })

    })
      .then(res => res.json())
      .then(data => {
        // console.log('data :', data);
        //Refer to part 2 of this Google Login step by step our server side.

        //After receiving the return from our API server:

        //we will show alerts to show if the user logged in properly or if there are errors.
        if (typeof data.accessToken !== 'undefined') {

          //set the accessToken into our localStorage as token:
          localStorage.setItem('token', data.accessToken)

          //run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
          fetch(`${server}/api/users/details`, {

            headers: {

              'Authorization': `Bearer ${data.accessToken}`
            }

          })
            .then(res => res.json())
            .then(data => {

              localStorage.setItem('email', data.email)
              localStorage.setItem('isAdmin', data.isAdmin)
              localStorage.setItem("id", data._id);
              //after getting the user's details, update the global user state:
              setUser({

                email: data.email,
                id: data._id

              })

              //Fire a sweetalert to inform the user of successful login:
              Swal.fire({

                icon: 'success',
                title: 'Successful Login'

              })

              redirect()
            })

        } else {

          //if data.accessToken is undefined, therefore, data contains a property called error instead.

          //This error will be shown if somehow our user's google token has an error or is compromised.
          if (data.error === "google-auth-error") {

            Swal.fire({

              icon: 'error',
              title: 'Google Authentication Failed'


            })

          } else if (data.error === "login-type-error") {

            //This error will be shown if our user has already created an account in our app using the register page but is trying to use google login to log into our app:

            Swal.fire({

              icon: 'error',
              title: 'Login Failed.',
              text: 'You may have registered through a different procedure.'

            })

          }

        }


      })

  }

  function failed(response) {
    // console.log(response)
  }

  /* ------------------------------ ------------ ------------------------------ */

  // console.log('userLogin :', user);

  //redirect logged in user


  //user input states
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //conditional
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]); //[email, password]

  async function authenticate(e) {
    e.preventDefault();
    await fetchLogin(email, password, setUser);
    setEmail("");
    setPassword("");
  }

  const pageVariants = {
    initial: {
      opacity: 1,
      x: "100vw",
      scale: 1,
    },
    in: {
      opacity: 1,
      x: "0",
      scale: 1,
    },
    out: {
      opacity: 1,
      x: "100vw",
      scale: 1,
    },
  };

  const pageTransition = {
    type: "twin",
    ease: "easeInOut",
    duration: 0.5,
  };

  const pageStyle = {
    position: "relative",
  };

  return user.email ? (
    // < Redirect to="/" />
    //<Redirect path="*" to="/" />
    redirect()
  )
    :
    (
      <motion.div
        style={pageStyle}
        initial="initial"
        animate="in"
        exit="out"
        variants={pageVariants}
        transition={pageTransition}
      >
        <Row className="d-flex justify-content-center">
          <Col md="4" className="registerCol rounded-lg">
            <div className="p-4">
              <h1 className="text-center text-white"> Login</h1>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                  <Form.Label className="text-white">Email:</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group controlId="userPassword">
                  <Form.Label className="text-white">Password:</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                {isActive ? (
                  <Button className="loginButton btn-block" type="submit">
                    Login
                  </Button>
                ) : (
                  <Button className="loginButton btn-block" type="submit" disabled>
                    Login
                  </Button>
                )}

                <GoogleLogin

                  clientId="526668376233-eudhqgtcmpaa940s30umhrnqvbpk8g4j.apps.googleusercontent.com"
                  buttonText="Login/Register Using Google"
                  onSuccess={authenticateGoogleToken}
                  onFailure={failed}
                  cookiePolicy={'single_host_origin'}
                  className="w-100 text-center my-4 d-flex justify-content-center"

                />
                <Link to="/register" className="text-white">
                  Don't have an account? Register here
                </Link>


              </Form>
            </div>
          </Col>
        </Row>
      </motion.div>
    );
}
