//import/export global method/property TO REACT(inside a react function)

import React from "react";

// context
const UserContext = React.createContext();
export default UserContext;

// Provider
export const UserProvider = UserContext.Provider;
