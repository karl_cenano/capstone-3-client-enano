import React from "react";
import "./../App.css";

import { useState, useEffect, useContext } from "react";
import UserContext from "../userContext";
import {
    Row,
    Col,
    Container,
    Card,
} from "react-bootstrap";
import { motion } from "framer-motion";


import fetchIncomeList from '../backendFetch/Budget-tracker/incomeList'
import fetchExpensesList from '../backendFetch/Budget-tracker/expensesList'
import fetchTotalExpenses from '../backendFetch/Budget-tracker/totalExpenses'
import fetchTotalIncome from '../backendFetch/Budget-tracker/totalIncome'

import BarChartMonths from './../components/graphs/BarChartMonths';
import PieChartIncome from '../components/graphs/PieChartIncome'
import PieChartExpenses from '../components/graphs/PieChartExpenses'
import SplineChartTrend from './../components/graphs/SplineChartTrend';


export default function Analytics() {

    let incomeListCategories;
    let incomeSummaryCategories = [];
    let expensesSummaryCategories = [];
    let expensesListCategories;
    let incomeListMapForCombine = [];
    let expensesListMapForCombine = [];

    const { user } = useContext(UserContext);

    const [incomeList, setIncomeList] = useState([]);
    // console.log('incomeList :', incomeList);
    const [expensesList, setExpensesList] = useState([]);
    // console.log('expensesList :', expensesList);    
    const [totalExpenses, setTotalExpenses] = useState([]);
    const [totalIncome, setTotalIncome] = useState([]);


    /* ----------------------- line chart ---------------------- */



    if (incomeList.length > 0) {
        incomeListMapForCombine = incomeList.map((result) => {

            return (
                {
                    date: result.dateAdded,
                    balance: result.incomePrice,

                }
            );
        });
    }
    // console.log('incomeListMapForCombine :', incomeListMapForCombine);

    if (expensesList.length > 0) {
        expensesListMapForCombine = expensesList.map((result) => {

            return (
                {
                    date: result.dateAdded,
                    balance: -result.expensesPrice,

                }
            );
        });
    }
    //console.log('expensesListMapForCombine :', expensesListMapForCombine);
    const combined = [...incomeListMapForCombine, ...expensesListMapForCombine]
    //console.log('combined :', combined);

    let resultCombined = Object.values(combined.reduce((a, { date, balance }) => {
        a[date] = (a[date] || { date, balance: 0 });
        a[date].balance = String(Number(a[date].balance) + Number(balance))
        return a;
    }, {}));

    // console.log("resultCombined", resultCombined);

    let resultCombinedSorted = resultCombined.sort(function (a, b) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(a.date) - new Date(b.date);
    });

    // console.log('resultCombinedSorted :', resultCombinedSorted);

    let resultCombinedSortedFormatted = resultCombinedSorted.map((result) => {

        return (
            {
                date: result.date,
                balance: result.balance,

            }
        );
    });

    //console.log('resultCombinedSortedFormatted :', resultCombinedSortedFormatted);

    /* ---------------------- ---------------------------- ---------------------- */








    useEffect(() => {
        fetchTotalExpenses(user.id, user.token, setTotalExpenses);
    }, [/* totalExpenses */]);

    useEffect(() => {
        fetchTotalIncome(user.id, user.token, setTotalIncome);
    }, [/* totalIncome */]);

    useEffect(() => {
        fetchIncomeList(user.id, user.token, setIncomeList);
    }, [/* incomeList */]); //!performance issue

    useEffect(() => {
        fetchExpensesList(user.id, user.token, setExpensesList);
    }, [/* expensesList */]); //!performance issue

    const pageVariants = {
        initial: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
        in: {
            opacity: 1,
            x: 0,
            scale: 1,
        },
        out: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
    };

    const pageTransition = {
        type: "tween",
        ease: "easeInOut",
        duration: 0.5,
    };

    const pageStyle = {
        position: "relative",
    };

    /* ---------------------------- income categories --------------------------- */

    if (incomeList.length > 0) {
        incomeListCategories = incomeList.map((result) => {
            //console.log(result)

            return {
                category: result.category,
                income: result.incomePrice
            }

        })
    } else {
        incomeListCategories = ""
    }
    //console.log('incomeListCategories :', incomeListCategories); //o: array

    if (incomeListCategories.length > 0) {
        incomeSummaryCategories = Array.from(incomeListCategories.reduce(
            (m, { category, income }) => m.set(category, (m.get(category) || 0) + income), new Map
        ), ([category, income]) => ({ category, income }));
    }


    /* ---------------------------- expenses categories --------------------------- */

    if (expensesList.length > 0) {
        expensesListCategories = expensesList.map((result) => {

            return {
                category: result.category,
                expenses: result.expensesPrice
            }

        })
    } else {
        expensesListCategories = ""
    }

    //console.log('expensesListCategories :', expensesListCategories); //o: array
    if (expensesListCategories.length > 0) {
        expensesSummaryCategories = Array.from(expensesListCategories.reduce(
            (m, { category, expenses }) => m.set(category, (m.get(category) || 0) + expenses), new Map
        ), ([category, expenses]) => ({ category, expenses }));
    }



    //^ --------------------------------- RETURN --------------------------------- */


    return (
        <motion.div
            style={pageStyle}
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={pageTransition}
        >
            <Container fluid>
                <Row className="mb-4">
                    <Col>
                        <Card className="splineChart "><SplineChartTrend props={resultCombinedSortedFormatted} /></Card>

                    </Col>
                </Row>
                <Row>

                    <Col >
                        <Card className="analyticsCard text-white"><Card.Title>Monthly Income </Card.Title><BarChartMonths props={incomeList} /></Card>
                    </Col>
                    <Col >
                        <Card className="analyticsCard text-white"><Card.Title>Monthly Expenses </Card.Title><BarChartMonths props={expensesList} /></Card>
                    </Col>
                    <Col >
                        <Card className="analyticsCard text-white"><Card.Title>Income Category Breakdown</Card.Title><PieChartIncome props={incomeSummaryCategories} /> </Card>
                    </Col>
                    <Col >
                        <Card className="analyticsCard text-white"><Card.Title>Expenses Category Breakdown</Card.Title> <PieChartExpenses props={expensesSummaryCategories} /> </Card>
                    </Col>
                </Row>

            </Container >
        </motion.div>



    )
}
