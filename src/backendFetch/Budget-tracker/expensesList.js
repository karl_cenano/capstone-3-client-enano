import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";

/* ---------------------------- public function ---------------------------- */

export default function fetchExpensesList(id, token, setExpensesList) {
  fetch(`${server}/api/users/expensesList`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      id: id,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      //console.log("fetchsetIncomeList: ", data);
      setExpensesList(data);
    });
}
