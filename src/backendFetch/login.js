import React from "react";
import Swal from "sweetalert2";
import server from "./server";
import { Redirect } from "react-router-dom";
import { GoogleLogin } from 'react-google-login'
/* ---------------------------- public function ---------------------------- */

export default function fetchLogin(email, password, setUser) {
  fetch(`${server}/api/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
    .then((res) => res.json())
    .then((data) => {

      if (data.accessToken) {
        localStorage.setItem("token", data.accessToken);

        //local function
        fetchdetails(data, setUser);

        // Swal.fire({
        //   icon: "success",
        //   title: "Successfully Logged In.",
        //   text: "Thank you for logging in.",
        // });
        //Router.push('/') //nextjs router component like a window replace
      } else {
        Swal.fire({
          icon: "error",
          title: "Log in failed.",
          text: "User authentication failed.",
        });
      }
    });
}

/* ----------------------------- private function ---------------------------- */

export let fetchdetails = (data, setUser) => {
  fetch(`${server}/api/users/details`, {
    headers: {
      Authorization: `Bearer ${data.accessToken}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      //console.log("login11: ", data);
      localStorage.setItem("email", data.email);
      localStorage.setItem("id", data._id);

      //! REVIEW On log in already sets email and isAdmin to a value
      setUser({
        email: data.email,
        id: data._id,
      });
    })
};
