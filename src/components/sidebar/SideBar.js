import React from "react";

import { useState, useEffect, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
} from "@fortawesome/free-solid-svg-icons";
import { NavItem, NavLink, Nav } from "reactstrap";
//import { Navbar, Nav } from 'react-bootstrap'
import classNames from "classnames";
import { Link, NavLink as RouterNavLink } from "react-router-dom";


import { useMedia } from "react-media";
import SubMenu from "./SubMenu";
import UserContext from "../../userContext";
import "../../App.css";
import Icon from "../../images/LogoMakr1.png"
/* ------------------------ react-media: window size ------------------------ */

const SideBar = ({ isOpen, toggle }) => {
  /* ------------------------------- window size ------------------------------ */

  //console.log('Link :', Link);
  const GLOBAL_MEDIA_QUERIES = {
    small: "(max-width: 599px)",
    large: "(min-width: 1366px)"
  };
  const matches = useMedia({ queries: GLOBAL_MEDIA_QUERIES });
  //console.log('matches :', matches);

  /* ------------------------------ global props ------------------------------ */
  const { user, setUser } = useContext(UserContext);
  //console.log("userLogin :", user);

  return (
    <>
      <div className={classNames("sidebar", { "is-open": isOpen })}>
        <div className="sidebar-header">
          <span color="info" onClick={toggle} style={{ color: "#fff" }}>
            &times;
          </span>

          <h3 className="font-bold headerIcon">INCOMIFY</h3>
        </div>
        <div className="side-menu">

          <Nav vertical className="list-unstyled pb-3">

            <p className="sidebarDashboard">Dashboard</p>
            <NavItem className="sidebarNavItem">
              <Link
                onClick={matches.small ? toggle : null}
                className="sidebarlinks"
                to="/"
              >
                <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                Home
              </Link>
            </NavItem>


            {
              !user.email ? (
                <>
                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to={"/register"}
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                    Register
                  </Link>
                  </NavItem>

                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to={"/login"}
                    >
                      <FontAwesomeIcon icon={faQuestion} className="mr-2" />
                    Login
                  </Link>
                  </NavItem>

                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to="/about"
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                    About
                    </Link>
                  </NavItem>

                </>
              ) : (
                <>
                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to={"/analytics"}
                    >
                      <FontAwesomeIcon icon={faImage} className="mr-2" />
                      Analytics
                   </Link>
                  </NavItem>

                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to="/records"
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                    Records
                    </Link>
                  </NavItem>
                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to="/profile">
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                      Profile
                  </Link>
                  </NavItem>

                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to="/about"
                    >
                      <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
                    About
                    </Link>
                  </NavItem>


                  <NavItem className="sidebarNavItem">
                    <Link
                      onClick={matches.small ? toggle : null}
                      className="sidebarlinks"
                      to={"/logout"}
                    >
                      <FontAwesomeIcon icon={faQuestion} className="mr-2" />
                    Logout
                  </Link>
                  </NavItem>
                </>
              )
            }
          </Nav >
        </div >
      </div >
    </>
  );
};


export default SideBar;








 // <>
    //   <div className={classNames("sidebar", { "is-open": isOpen })}>
    //     <div className="sidebar-header">
    //       <span color="info" onClick={toggle} style={{ color: "#fff" }}>
    //         &times;
    //       </span>
    //       <h3 className="font-italic">BudgetGO!</h3>
    //     </div>
    //     <div className="side-menu">
    //       <Navbar vertical="true" className="list-unstyled pb-3">

    //         <Nav.Link
    //           href="/"
    //         >
    //           <FontAwesomeIcon icon={faBriefcase} className="mr-2" />
    //             Home
    //           </Nav.Link>
    //       </Navbar>
    //     </div>
    //   </div>
    // </>