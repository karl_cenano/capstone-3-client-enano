import "../../App.css";
import React from "react";
import { useState, useEffect, useContext } from "react";
import UserContext from "../../userContext";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignLeft } from "@fortawesome/free-solid-svg-icons";
import {
  Navbar,
  Button,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import { useMedia } from "react-media";

const Topbar = ({ toggleSidebar }) => {

  const { user, setUser } = useContext(UserContext);

  /* --------------------------------- states --------------------------------- */

  const [topbarIsOpen, setTopbarOpen] = useState(true);
  const toggleTopbar = () => setTopbarOpen(!topbarIsOpen);

  /* ---------------------------------- media---------------------------------- */

  const GLOBAL_MEDIA_QUERIES = {
    large: "(min-width: 1365px)",
    small: "(max-width: 500px)"
  };
  const matches = useMedia({ queries: GLOBAL_MEDIA_QUERIES });





  //^ --------------------------------- return --------------------------------- */

  return (
    <Navbar light className="navbar mb-5 topBar rounded" expand="sm">
      {matches.large ?
        <></>
        :
        <Button className="toggleSidebar" onClick={toggleSidebar}>
          <FontAwesomeIcon icon={faAlignLeft} />
        </Button>
      }
      {matches.large || matches.small ? (
        <br />
      ) : (
        <>
          <NavbarToggler onClick={toggleTopbar} />
          <Collapse isOpen={topbarIsOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to={"/"} className="text-white ml-3 mr-3">
                  Home
                </Link>
              </NavItem>

              {!user.email ?
                <>
                  <NavItem>
                    <Link to={"/register"} className="text-white ml-3 mr-3">
                      Register
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link to={"/login"} className="text-white ml-3 mr-3">
                      Login
                    </Link>
                  </NavItem>
                </>
                :
                <>
                  <NavItem>
                    <Link to={"/analytics"} className="text-white ml-3 mr-3">
                      Analytics
                  </Link>
                  </NavItem>
                  <NavItem>
                    <Link to={"/records"} className="text-white ml-3 mr-3">
                      Records
                  </Link>
                  </NavItem>
                  <NavItem>
                    <Link to={"/logout"} className="text-white ml-3 mr-3">
                      Logout
                  </Link>
                  </NavItem>
                </>
              }

            </Nav>
          </Collapse>
        </>
      )}
    </Navbar>
  );
};

export default Topbar;
