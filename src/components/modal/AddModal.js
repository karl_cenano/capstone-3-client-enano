import React from 'react';
import "../../App.css"
import { Modal, Button, Form, DropdownButton, Dropdown } from 'react-bootstrap'
import { useState, useEffect, useContext } from "react";
import UserContext from "../../userContext";

import fetchAddIncome from '../../backendFetch/Budget-tracker/addIncome'
import fetchAddExpenses from '../../backendFetch/Budget-tracker/addExpenses'

export default function AddModal(props) {

    const { user, setUser } = useContext(UserContext);

    const [stateSelect, setStateSelect] = useState(false)
    const [toggleForm, setToggleForm] = useState(0)
    const [income, setIncome] = useState(0)
    const [expenses, setExpenses] = useState(0)
    const [description, setDescription] = useState("")
    const [category, setCategory] = useState("")

    const [fetchAddIncomeVar, setFetchAddIncome] = useState(false)
    const [fetchAddExpensesVar, setFetchAddExpenses] = useState(false)
    //console.log('fetchAddIncomeVar :', fetchAddIncomeVar);



    //conditional
    const [isActive, setIsActive] = useState(true);

    /* -------------------------------- useEffect ------------------------------- */

    useEffect(() => {
        if ((income !== 0 || expenses !== 0) && description !== "" && category !== "") {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [income, expenses, description, category]);

    /* -------------------------------- functions ------------------------------- */

    function toggle1(e) {

        setStateSelect("Income")
        setToggleForm(1)
    }
    function toggle2(e) {
        setStateSelect("Expenses")
        setToggleForm(2)
    }

    async function addIncomeFunction(e) {
        e.preventDefault();
        setIncome(0)
        setDescription("")
        setCategory("")
        await fetchAddIncome(user.id, user.token, income, description, category, setFetchAddIncome)

    }

    async function addExpensesFunction(e) {
        e.preventDefault();
        setIncome(0)
        setDescription("")
        setCategory("")
        await fetchAddExpenses(user.id, user.token, expenses, description, category, setFetchAddExpenses)

    }



    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"

        >
            <Modal.Header className="modalBody" closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className="text-white">
                    Add Income/Expense
          </Modal.Title>
            </Modal.Header>
            <Modal.Body className="modalBody" >
                <DropdownButton
                    variant="outline-secondary"
                    title="Select Income/Expenses"
                    id="input-group-dropdown-1">
                    <Dropdown.Item onSelect={toggle1}>Income</Dropdown.Item>
                    <Dropdown.Item onSelect={toggle2}>Expenses</Dropdown.Item>
                </DropdownButton>
                {(toggleForm === 1) ?
                    <>
                        <Form onSubmit={(e) => addIncomeFunction(e)}>
                            <Form.Group controlId="Income">
                                <Form.Label className="text-white mt-4">Income: </Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Income"
                                    value={income}
                                    onChange={(e) => setIncome(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="IncomeDescription">
                                <Form.Label className="text-white">Description: </Form.Label>
                                <Form.Control
                                    type="string"
                                    placeholder="Enter Description"
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="Income Category">
                                <Form.Label className="text-white">Category: </Form.Label>
                                <Form.Control
                                    type="string"
                                    placeholder="Enter Category Name"
                                    value={category}
                                    onChange={(e) => setCategory(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            {isActive ? (
                                <Button className="buttonPurple" type="submit">
                                    Add Income
                                </Button>
                            ) : (
                                <Button className="buttonPurple" type="submit" disabled>
                                    Add Income
                                </Button>
                            )}
                        </Form>

                    </>
                    :
                    <>
                    </>
                }
                {(toggleForm === 2) ?
                    <>
                        <Form onSubmit={(e) => addExpensesFunction(e)}>
                            <Form.Group controlId="Expenses">
                                <Form.Label className="text-white mt-4" >Expenses: </Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Expenses"
                                    value={expenses}
                                    onChange={(e) => setExpenses(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="ExpensesDescription">
                                <Form.Label className="text-white">Description:</Form.Label>
                                <Form.Control
                                    type="string"
                                    placeholder="Enter Description"
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="Expenses Category">
                                <Form.Label className="text-white">Category: </Form.Label>
                                <Form.Control
                                    type="string"
                                    placeholder="Enter Category Name"
                                    value={category}
                                    onChange={(e) => setCategory(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            {isActive ? (
                                <Button className="buttonPurple" type="submit">
                                    Add Expenses
                                </Button>
                            ) : (
                                <Button className="buttonPurple" type="submit" disabled>
                                    Add Expenses
                                </Button>
                            )}
                        </Form>

                    </>
                    :
                    <>

                    </>
                }

            </Modal.Body>
            <Modal.Footer className="modalBody">
                <Button onClick={props.onHide} className="buttonPurple">Close</Button>
            </Modal.Footer>
        </Modal>
    );
}