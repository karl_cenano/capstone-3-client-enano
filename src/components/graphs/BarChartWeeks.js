import React from "react";
import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import { render } from 'react-dom'



export default function BarChartWeeks({ rawData }) {

    //console.log(rawData)
    let rawDataArray = Object.values(rawData)
    //console.log('rawDataArray :', rawDataArray);


    const data = {
        labels: ['1', '2', '3', '4', '5'],
        datasets: [{
            label: 'week',
            backgroundColor: 'purple',
            borderColor: 'purple',
            borderWidth: 1,
            hoverBackgroundColor: 'lightblue',
            hoverBorderColor: 'black',
            data: [rawData.week1, rawData.week2, rawData.week3, rawData.week4, rawData.week5]
        }]


    }

    const options = {
        legend: {
            display: false
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        display: false
                    },
                    gridLines: {
                        display: false
                    }

                }
            ],
            xAxes: [{
                categoryPercentage: 1.0,
                barPercentage: 0.2,
                gridLines: {
                    display: false
                }
            }],
            x: {
                grid: {
                    offset: true
                }
            }
        }
    }


    return (
        <Bar data={data} options={options} />
    )
}