import React from "react";
import Swal from "sweetalert2";
import server from "../server";
import { Redirect } from "react-router-dom";
import { useState, useEffect, useContext } from "react";

/* ---------------------------- public function ---------------------------- */

export const AutoRender = false;

export default function fetchAddExpenses(id, token, expenses, description, category, setFetchAddExpenses) {
    fetch(`${server}/api/users/addExpenses`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
            id: id,
            expenses: expenses,
            description, description,
            category: category
        }),
    })
        .then((res) => res.json())
        .then((data) => {
            //console.log("fetchTotalIncomeData: ", data);
            if (data === true) {
                setFetchAddExpenses(data);

                Swal.fire({
                    icon: "success",
                    title: "Successfully Added.",
                    text: "Added to Income.",
                });

            } else {
                Swal.fire({
                    icon: "error",
                    title: "Failed.",
                    text: "Fail to add.",
                });
            }
        })



}
