import React from "react";
import { useState, useEffect, useContext } from "react";
import UserContext from "../userContext";
import { Redirect } from "react-router-dom";

export default function Logout() {
  const { unsetUser } = useContext(UserContext);

  useEffect(() => {
    unsetUser();
    // console.log("unsetUser", unsetUser);
    //Router.push('/login')
  });

  return <Redirect to="/login" />;
}
