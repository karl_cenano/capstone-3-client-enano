/*//^ ------------------------------ declarations ------------------------------ */
import "./../App.css";

import React, { useState, useEffect, useContext } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import fetchRegister from "../backendFetch/register";
import { motion } from "framer-motion";

/*//^ -------------------------------- component ------------------------------- */

export default function Register() {
  // console.log(UserContext)
  // console.log('UseContext', useContext(UserContext))
  //state for active/inactive element

  /*//^ --------------------------------- states --------------------------------- */

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState(0);
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(true);

  /*//^ --------------------------------- effect --------------------------------- */

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 == password2 &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  /*//^ -------------------------------- function -------------------------------- */

  async function registerUser(e) {
    e.preventDefault();

    await fetchRegister(firstName, lastName, email, mobileNo, password1);
    //clear input fields
    setFirstName("");
    setLastName("");
    setEmail("");
    setMobileNo("");
    setPassword1("");
    setPassword2("");
  }

  const pageVariants = {
    initial: {
      opacity: 1,
      x: "100vh",
      scale: 1,
    },
    in: {
      opacity: 1,
      x: 0,
      scale: 1,
    },
    out: {
      opacity: 1,
      x: "100vh",
      scale: 1,
    },
  };

  const pageTransition = {
    type: "tween",
    ease: "easeInOut",
    duration: 0.5,
  };

  const pageStyle = {
    position: "relative",
  };
  /*//^ --------------------------------- return --------------------------------- */

  return (
    <>
      <motion.div
        style={pageStyle}
        initial="initial"
        animate="in"
        exit="out"
        variants={pageVariants}
        transition={pageTransition}
      >
        <Row className="d-flex justify-content-center">
          <Col md="4" className="registerCol rounded-lg">
            <div className="p-4">
              <h1 className="text-center text-white"> Register</h1>
              <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="userFirstName">
                  <Form.Label className="text-white">First Name</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="text"
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={(e) => {
                      setFirstName(e.target.value);
                    }}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="userLastName">
                  <Form.Label className="text-white">Last Name</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="text"
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={(e) => {
                      setLastName(e.target.value);
                    }}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="userEmail">
                  <Form.Label className="text-white">Email</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="text"
                    placeholder="Enter Email"
                    value={email}
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="userMobile">
                  <Form.Label className="text-white">Mobile No</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="text"
                    placeholder="Enter Mobile No"
                    value={mobileNo}
                    onChange={(e) => {
                      setMobileNo(e.target.value);
                    }}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="userPassword1">
                  <Form.Label className="text-white">Password</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="password"
                    placeholder="Enter Password"
                    value={password1}
                    onChange={(e) => {
                      setPassword1(e.target.value);
                    }}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="userPassword2">
                  <Form.Label className="text-white">Confirm Password</Form.Label>
                  <Form.Control
                    className="shadow border-0"
                    type="password"
                    placeholder="Enter Password"
                    value={password2}
                    onChange={(e) => {
                      setPassword2(e.target.value);
                    }}
                    required
                  />
                </Form.Group>

                {isActive ? (
                  <Col md="12" className="mt-5">
                    <Button
                      variant="secondary"
                      className="regButton btn-block"
                      type="submit"
                    >
                      Submit
                    </Button>
                  </Col>
                ) : (
                  <Col md="12" className="mt-5">
                    <Button
                      variant="transparent"
                      className="border btn-block"
                      disabled
                    >
                      Submit
                    </Button>
                  </Col>
                )}
              </Form>
            </div>
          </Col>
        </Row>
      </motion.div>
    </>
  );
}
