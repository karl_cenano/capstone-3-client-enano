/* ------------------------------ declarations ------------------------------ */

import "./../App.css";

import React from "react";
import { useState, useEffect, useContext } from "react";

import Banner from "./../components/banner/Banner";
import CardList from "./../components/cardList/CardList";
import CardListCategories from "./../components/cardList/CardListCategories"
import IncomeListScroll from "./../components/scrollbar/IncomeListScroll";
import ExpensesListScroll from "./../components/scrollbar/ExpensesListScroll";
import AddModal from "./../components/modal/AddModal"
import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';

import { motion } from "framer-motion";
import UserContext from "../userContext";
import {
    Button,
    Row,
    Col,
    Container,
} from "react-bootstrap";

import fetchTotalExpenses from "../backendFetch/Budget-tracker/totalExpenses";
import fetchTotalIncome from "../backendFetch/Budget-tracker/totalIncome";
import fetchIncomeList from "../backendFetch/Budget-tracker/incomeList";
import fetchExpensesList from "../backendFetch/Budget-tracker/expensesList";


import MonthOverviewIncome from '../components/cardList/MonthOverviewIncome';
import MonthOverviewExpenses from '../components/cardList/MonthOverviewExpenses';



/*//^ ---------------------------------- props --------------------------------- */
let incomeListMap;
let expensesListMap;
let incomeListCategories;
let summaryCategories;
let summaryIncomeCategoriesMap;
let summaryExpensesCategoriesMap;
let expensesListCategories;
let expensesSummaryCategories;

let data2 = {
    title: "Track Your Expenses Without Any Hassle",
    description: "Easily track your expenses",
    motto: "On the GO!",
    destination: "/login",
    label: "List it Now!",
};




/*//^ -------------------------------- component ------------------------------- */

export default function Home() {
    const { user, setUser } = useContext(UserContext);

    const [totalExpenses, setTotalExpenses] = useState([]);
    const [totalIncome, setTotalIncome] = useState([]);
    const [addModalState, setAddModalState] = useState(false);
    const [incomeList, setIncomeList] = useState([]);
    const [expensesList, setExpensesList] = useState([]);

    /* ------------------------------ fetch initial ----------------------------- */

    useEffect(() => {
        fetchTotalExpenses(user.id, user.token, setTotalExpenses);
    }, [incomeList]);
    // console.log('totalExpenses :', totalExpenses);

    useEffect(() => {
        fetchTotalIncome(user.id, user.token, setTotalIncome);

    }, []);

    useEffect(() => {
        fetchIncomeList(user.id, user.token, setIncomeList);
    }, [incomeList]); //!performance issue

    useEffect(() => {
        fetchExpensesList(user.id, user.token, setExpensesList);
    }, [expensesList]); //!performance issue



    /* -------------------------------- incomelist ------------------------------- */
    function reverseArr(input) {
        var ret = new Array;
        for (var i = input.length - 1; i >= 0; i--) {
            ret.push(input[i]);
        }
        return ret;
    }

    if (incomeList.length > 0) {
        incomeListMap = incomeList.map((result) => {
            var created_date = new Date(result.dateAdded);
            var year = created_date.getFullYear();
            var month = created_date.getMonth() + 1;
            var date = created_date.getDate();
            var hour = created_date.getHours();
            var min = created_date.getMinutes();
            var sec = created_date.getSeconds();
            var time =
                month + "-" + date + "-" + year + " " + hour + ":" + min + ":" + sec;
            result.time = time;

            return <CardList key={result._id} listProp={result} />;
        });

    } else {
        incomeListMap = "";

    }

    let reverseMapIncome = reverseArr(incomeListMap);

    /* ------------------------------ expenses list ----------------------------- */


    if (expensesList.length > 0) {
        expensesListMap = expensesList.map((result) => {
            var created_date = new Date(result.dateAdded);

            var year = created_date.getFullYear();
            var month = created_date.getMonth();
            var date = created_date.getDate();
            var hour = created_date.getHours();
            var min = created_date.getMinutes();
            var sec = created_date.getSeconds();
            var time =
                month + "-" + date + "-" + year + " " + hour + ":" + min + ":" + sec;
            result.time = time;

            return <CardList key={result._id} listProp={result} />;
        });
    } else {
        expensesListMap = "";
    }

    let reverseMapExpenses = reverseArr(expensesListMap);

    /* ---------------------------- income categories --------------------------- */

    if (incomeList.length > 0) {
        incomeListCategories = incomeList.map((result) => {

            return {
                category: result.category,
                income: result.incomePrice
            }

        })
    } else {
        incomeListCategories = ""
    }

    //console.log('incomeListCategories :', incomeListCategories); //o: array
    if (incomeListCategories.length > 0) {
        summaryCategories = Array.from(incomeListCategories.reduce(
            (m, { category, income }) => m.set(category, (m.get(category) || 0) + income), new Map
        ), ([category, income]) => ({ category, income }));
        //console.log(summaryCategories);

        if (summaryCategories.length > 0) {
            summaryIncomeCategoriesMap = summaryCategories.map((result, index) => {
                result.totalIncome = totalIncome.totalIncome;

                //console.log(result)
                return <CardListCategories key={index} listProp={result} />;
            })
        }
    }


    /* ---------------------------- expenses categories --------------------------- */

    if (expensesList.length > 0) {
        expensesListCategories = expensesList.map((result) => {

            return {
                category: result.category,
                expenses: result.expensesPrice
            }

        })
    } else {
        expensesListCategories = ""
    }

    //console.log('expensesListCategories :', expensesListCategories); //o: array
    if (expensesListCategories.length > 0) {
        expensesSummaryCategories = Array.from(expensesListCategories.reduce(
            (m, { category, expenses }) => m.set(category, (m.get(category) || 0) + expenses), new Map
        ), ([category, expenses]) => ({ category, expenses }));
        //console.log(summaryCategories);

        if (expensesSummaryCategories.length > 0) {
            summaryExpensesCategoriesMap = expensesSummaryCategories.map((result, index) => {
                result.totalExpenses = totalExpenses.totalExpenses;

                // console.log(result)

                return <CardListCategories key={index} listProp={result} />;
            })
        }
    }





    /* --------------------------------- search --------------------------------- */

    const [searchTerm, setSearchTerm] = React.useState("");
    const handleChange = (event) => {
        setSearchTerm(event.target.value);
    };

    /* ------------------------------- Animations ------------------------------- */

    const pageVariants = {
        initial: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
        in: {
            opacity: 1,
            x: 0,
            scale: 1,
        },
        out: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
    };

    const pageTransition = {
        type: "tween",
        ease: "easeInOut",
        duration: 0.5,
    };

    const pageStyle = {
        position: "relative",
    };


    //^ --------------------------------- RETURN --------------------------------- */

    return !user.email ? (
        <>
            <motion.div
                style={pageStyle}
                initial="initial"
                animate="in"
                exit="out"
                variants={pageVariants}
                transition={pageTransition}
            >
                <Banner dataProp={data2} />
            </motion.div>
        </>
    ) : (
        <motion.div
            style={pageStyle}
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={pageTransition}
        >
            <Container fluid>
                <Row>
                    <Col lg="7" className="mr-5">
                        <Row>
                            <Col lg="6">
                                <h3 className="text-white mt-2 mb-4">Transactions</h3>
                            </Col>
                            <Col lg="4"> </Col>
                            <Col md="3">
                                <Button variant="primary" onClick={(e) => setAddModalState(true)} className="buttonPurple"> Add Income/Expenses</Button>
                                <AddModal onHide={() => setAddModalState(false)} show={addModalState} />
                            </Col>
                        </Row>


                        <Row className="mt-5 mb-3">
                            <Col lg="3" className="text-white">
                                {" "}
                                <div>Total Income</div>
                                <h3>PHP {totalIncome.totalIncome}</h3>
                            </Col>
                            <Col lg="3" className="text-white">
                                {" "}
                                <div>Total Expenses</div>
                                <h3>PHP {totalExpenses.totalExpenses}</h3>
                            </Col>

                            <Col lg="3"></Col>
                            <Col lg="3" className="text-white">
                                {" "}
                                <div>Net Balance</div>
                                <h3>
                                    PHP {(totalIncome.totalIncome - totalExpenses.totalExpenses).toFixed(0)}
                                </h3>
                            </Col>
                        </Row>
                        <Row className="cardHolder">
                            <Col md="12" className="">
                                <p className="cardHead"> Income List</p>
                                <IncomeListScroll props={reverseMapIncome} />
                            </Col>

                            <Col md="12">
                                <p className="cardHead"> Expenses List</p>
                                <ExpensesListScroll props={reverseMapExpenses} />
                            </Col>
                        </Row>
                    </Col>

                    <Col lg="4" className="lg-ml-5">
                        <div>
                            <h3 className="text-white mb-4"> This Month's Overview</h3>
                            <Row >
                                <Col md="6">
                                    <MonthOverviewIncome props={incomeList} />
                                </Col>
                                <Col md="6">
                                    <MonthOverviewExpenses props={expensesList} />
                                </Col>
                            </Row>
                            <p className="text-white mt-3">Income Categories</p>
                            <Row className=" categoriesOverview" >

                                <Col className="incomeCategoriesContainer"  >

                                    <PerfectScrollbar>{summaryIncomeCategoriesMap ? summaryIncomeCategoriesMap : <div></div>}</PerfectScrollbar> </Col>

                            </Row>
                            <p className="text-white mt-2 mb-2">Expenses Categories</p>
                            <Row className=" categoriesOverview">
                                <Col className="incomeCategoriesContainer" >  <PerfectScrollbar>{summaryExpensesCategoriesMap ? summaryExpensesCategoriesMap : <div></div>}</PerfectScrollbar></Col>
                            </Row>

                        </div>
                    </Col>
                </Row>
            </Container>
        </motion.div>
    );
}
