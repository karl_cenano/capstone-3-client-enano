import React, { Component } from "react";
import { useState, useEffect, useContext } from "react";
import ScrollMenu from "react-horizontal-scrolling-menu";
import "../../App.css";
import {
    Jumbotron,
    Button,
    Row,
    Col,
    Container,
    Card,
    Table,
} from "react-bootstrap";
import CardList from "../cardList/CardList";
import fetchExpensesList from "../../backendFetch/Budget-tracker/expensesList";
import UserContext from "../../userContext";

let expensesListMap;

// One item component
// selected prop will be passed
const MenuItem = ({ text, selected }) => {
    return <div className="menu-item text-white">{text}</div>;
};

// All items component
// Important! add unique key
export const Menu = (list) =>
    list.map((el) => {
        const { name } = el;

        return <MenuItem text={name} key={name} />;
    });

const Arrow = ({ text, className }) => {
    return <div className={className}>{text}</div>;
};

const ArrowLeft = Arrow({ text: "<", className: "arrow-prev text-white" });
const ArrowRight = Arrow({ text: ">", className: "arrow-next text-white" });

export default function ExpensesListScroll({ props }) {
    const { user, setUser } = useContext(UserContext);

    return (
        <div className="scrollbar">
            <ScrollMenu
                data={props}
                arrowLeft={ArrowLeft}
                arrowRight={ArrowRight}
                alignCenter={true}
                wheel={false}
                translate={0}
                clickWhenDrag={true}
            />
        </div>
    );
}
