import './../App.css'
import React from "react";

import NiceTable from '../components/table/NiceTable'
import AddModal from './../components/modal/AddModal';

import {
    Row,
    Col,
} from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../userContext";
import { Container } from 'react-bootstrap';

import fetchIncomeList from "../backendFetch/Budget-tracker/incomeList";
import fetchExpensesList from "../backendFetch/Budget-tracker/expensesList";
import { motion } from "framer-motion";

let incomeListMap = [];
let expensesListMap = [];
let list = [];

export default function Records() {
    const [addModalState, setAddModalState] = useState(false);
    const { user, setUser } = useContext(UserContext);

    const [incomeList, setIncomeList] = useState([]);
    const [expensesList, setExpensesList] = useState([]);

    useEffect(() => {
        fetchIncomeList(user.id, user.token, setIncomeList);
    }, [/* incomeList */]); //!performance issue

    useEffect(() => {
        fetchExpensesList(user.id, user.token, setExpensesList);
    }, [/* expensesList */]); //!performance issue

    const pageVariants = {

        initial: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
        in: {
            opacity: 1,
            x: 0,
            scale: 1,
        },
        out: {
            opacity: 0,
            x: "0vw",
            scale: 1,
        },
    };

    const pageTransition = {
        type: "tween",
        ease: "easeInOut",
        duration: 0.5,
    };

    const pageStyle = {
        position: "relative",
    };




    if (incomeList.length > 0) {
        incomeListMap = incomeList.map((result) => {
            var created_date = new Date(result.dateAdded);
            var year = created_date.getFullYear();
            var month = created_date.getMonth() + 1;
            var date = created_date.getDate();
            var hour = created_date.getHours();
            var min = created_date.getMinutes();
            var sec = created_date.getSeconds();
            var time =
                month + "-" + date + "-" + year + " " + hour + ":" + min + ":" + sec;
            result.time = time;

            return (
                {
                    description: result.description,
                    month: month,
                    year: year,
                    day: date,
                    category: result.category,
                    income: result.incomePrice
                }
            )
        });

    } else {
        incomeListMap = "";

    }


    if (expensesList.length > 0) {
        expensesListMap = expensesList.map((result) => {
            var created_date = new Date(result.dateAdded);

            var year = created_date.getFullYear();
            var month = created_date.getMonth() + 1;
            var date = created_date.getDate();
            var hour = created_date.getHours();
            var min = created_date.getMinutes();
            var sec = created_date.getSeconds();
            var time =
                month + "-" + date + "-" + year + " " + hour + ":" + min + ":" + sec;
            result.time = time;

            return (
                {
                    description: result.description,
                    month: month,
                    year: year,
                    day: date,
                    category: result.category,
                    expenses: result.expensesPrice
                }
            )
        });
    } else {
        expensesListMap = "";
    }
    //console.log('incomeListMap', incomeListMap)
    //console.log('expensesListMap', expensesListMap)

    if (incomeListMap.length > 0) {
        list = incomeListMap.concat(expensesListMap)
    }


    return (

        <motion.div
            style={pageStyle}
            initial="initial"
            animate="in"
            exit="out"
            variants={pageVariants}
            transition={pageTransition}
        >
            <Container fluid>
                {/* <Button variant="primary" onClick={(e) => setAddModalState(true)} className="buttonPurple"> Add</Button> */}
                <AddModal onHide={() => setAddModalState(false)} show={addModalState} />
                <Row>
                    <Col>
                        <NiceTable props={list} />
                    </Col>
                </Row>


            </Container>
        </motion.div>

    )
}