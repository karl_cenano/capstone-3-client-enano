import React from "react";
import "../../App.css";
import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";

export default function CardList({ listProp }) {
  // console.log(courseProp);
  return (
    <Col>
      <Card className="cardList">
        <Card.Body>
          <Card.Title>{listProp.description}</Card.Title>
          <Card.Text className="fontSmall">{listProp.time}</Card.Text>
          <Card.Text className="fontSmall">
            Category: {listProp.category}
          </Card.Text>
          <Card.Footer className=" ">
            PHP {listProp.incomePrice || listProp.expensesPrice}
          </Card.Footer>
        </Card.Body>
      </Card>
    </Col>
  );
}
